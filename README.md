# brokbitmnt

Yet Another Superfabulous Superimage Superglitcher = **YASSS**

## Requirements

- `ruby 2.6.3`
- `imagemagick`

## Usage

Run `gem install bundler` and then run `bundle install` to install gems from gemfile

Run 

```
bin/glitch <glitch_one>[:<glitch_two>] <file1> <file2>
```

where `<glitch_one>` and any following (separated by `:` ) are one of the file names (without extension) in `src/app/glitch/avi` or `src/app/glitch/png`
