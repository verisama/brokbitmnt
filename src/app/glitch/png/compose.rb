def png_compose(paths)
  images = []

  output_paths = Glitch::Png::glitch_many('compose', paths) do |image|
    image.width.times do |x|
      image.height.times do |y|
        pixel = image[x, y]
        image[x, y] = pixel + rand(3)
      end
    end

    images << image
  end

  puts "Processing composed image"

  png = ChunkyPNG::Image.new(512, 512, ChunkyPNG::Color::BLACK)

  images.each { |img| png.compose!(img, 0, 0) }

  filename = ['compose', '_', Time.now.strftime('%H%M%S'), '.png'].join
  png.save(File.join(Glitch::Png::OUTPUT_DIR, filename))

  output_paths.each { |path| File.delete(path) }

  puts "Done! :)"
end