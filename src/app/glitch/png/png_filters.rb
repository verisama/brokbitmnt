def png_filters(paths)
  filter = Glitch::Png::FILTERS[:avg]

  Glitch::Png::glitch_many_alt('png_filters', paths) do |image|
    image.each_scanline do |scanline|
      scanline.change_filter filter
    end
    image.glitch do |data|
      data.gsub! /[AEIOU]/, 'H'
      data.gsub! /[a-z]/, 'H'
    end
  end
end