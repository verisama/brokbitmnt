def png_colorful_squares(paths)
  Glitch::Png::glitch_many('colorful_squares', paths) do |image|
    mat = Matrix.build(image.width, image.height) do |x, y| 
      pixel = image[x, y]
      [
        ChunkyPNG::Color.r(pixel),
        ChunkyPNG::Color.g(pixel),
        ChunkyPNG::Color.b(pixel),
        ChunkyPNG::Color.a(pixel)
      ]
    end

    le_times = 20 + rand(50)
    
    puts "Le times: #{le_times}"
    
    le_times.times do |x|
      size = rand([image.width, image.height].min) / 4
      
      puts "  size ##{x}: #{size}"
      
      chunk_x = [0, [rand(image.width),  image.width  - size].min].max
      chunk_y = [0, [rand(image.height), image.height - size].min].max

      chunk_r = Matrix.build(size, size) { |x, y| mat[x + chunk_x, y + chunk_y][0] } 
      chunk_g = Matrix.build(size, size) { |x, y| mat[x + chunk_x, y + chunk_y][1] } 
      chunk_b = Matrix.build(size, size) { |x, y| mat[x + chunk_x, y + chunk_y][2] } 

      det_r = chunk_r.det
      det_g = chunk_g.det
      det_b = chunk_b.det

      chunk_r.each_with_index do |e, row, col|
        x = row + chunk_x
        y = col + chunk_y
        pixel = image[x, y]
        r, g, b = ChunkyPNG::Color.r(pixel), ChunkyPNG::Color.g(pixel), ChunkyPNG::Color.b(pixel)
        image[x, y] = ChunkyPNG::Color.rgb(r + det_r, g + det_g, b + det_b)
      end
    end
  end
end