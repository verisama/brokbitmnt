def png_bw_noise(paths)
  Glitch::Png::glitch_many('bw_noise', paths) do |image|
    channels = %i[r g b]
    wololo = false

    image.height.times do |y|
      image.width.times do |x|

        pixel = image[x, y]

        colors = {
          r: ChunkyPNG::Color.r(pixel),
          g: ChunkyPNG::Color.g(pixel),
          b: ChunkyPNG::Color.b(pixel)
        }

        colors = Hash[colors.map {|k,v| [k, 255 - v]}]

        channels = channels.shuffle


        image[x, y] = ChunkyPNG::Color.rgb(colors[channels[0]], colors[channels[1]], colors[channels[2]])
      end
    end
  end
end