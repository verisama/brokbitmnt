def png_vaporwave(paths)
  Glitch::Png::glitch_many('vaporwave', paths) do |image|  
    image.height.times do |y|
      the_number_is = rand(image.height) + 1
      
      image.width.times do |x|

        pixel = image[x, y]

        r, g, b = ChunkyPNG::Color.r(pixel), ChunkyPNG::Color.g(pixel), ChunkyPNG::Color.b(pixel)

        r += y % the_number_is
        g = g * g / image.width * 10

        image[x, y] = ChunkyPNG::Color.rgb(r, g, b)
      end
    end
  end
end