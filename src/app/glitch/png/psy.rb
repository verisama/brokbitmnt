def png_psy(paths)
  Glitch::Png::glitch_many('psy', paths) do |image|
    last = 255*3
    image.height.times do |y|
      the_number_is = rand(image.height) + 1
      
      image.width.times do |x|

        pixel = image[x, y]

        r, g, b = ChunkyPNG::Color.r(pixel), ChunkyPNG::Color.g(pixel), ChunkyPNG::Color.b(pixel)

        log = Math.log(the_number_is).to_i + 1 + x
        r += log + last / 3

        g = g * g / image.width * 10
        b = (x + y) / 4  
        
        last = r+g+b
        
        image[x, y] = ChunkyPNG::Color.rgb(r, g, b)
      end
    end
  end
end