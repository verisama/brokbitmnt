def png_grainy_lines(paths)
  Glitch::Png::glitch_many('grainy_lines', paths) do |image|
    image.height.times do |y|
      the_number_is = rand(image.height)
      
      image.width.times do |x|

        pixel = image[x, y]
        r, g, b = ChunkyPNG::Color.r(pixel), ChunkyPNG::Color.g(pixel), ChunkyPNG::Color.b(pixel)

        image[x, y] = image[x, the_number_is] if (r + g + b) % 8 == 0
      end
    end
  end
end