def png_yellow_contrast(paths)
  Glitch::Png::glitch_many('yellow_contrast', paths) do |image|
    the_number_is = rand(image.height)
    
    image.height.times do |y|
      image.width.times do |x|

        pixel       = image[x, y]
        pixel_other = image[x, the_number_is]

        r1, g1, b1 = ChunkyPNG::Color.r(pixel), ChunkyPNG::Color.g(pixel), ChunkyPNG::Color.b(pixel)
        r2, g2, b2 = ChunkyPNG::Color.r(pixel_other), ChunkyPNG::Color.g(pixel_other), ChunkyPNG::Color.b(pixel_other)

        image[x, y] = ChunkyPNG::Color.rgb((r1 + r2) / 2, g1 - g2, b1 + b2)
      end
    end
  end
end