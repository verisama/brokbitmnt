def avi_fun(paths)
  Glitch::Avi::glitch_many('fun', paths) do |video|
    video.glitch(:keyframe) do |data|
      data.gsub(/[abcde]/, 'f')
      data.gsub(/[123]/, '9')
    end
  end
end
