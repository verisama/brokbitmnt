def avi_bla(paths)
  Glitch::Avi::glitch_many('bla', paths) do |video|
    d = []

    video.frames.each_with_index do |f, i|
      d.push(i) if f.is_deltaframe?     # Collecting non-keyframes indices.
    end
    
    q = video.frames[0, 5]                  # Keep first key frame.
    
    100.times do
      x = video.frames[d[rand(d.size)], 1]  # Select a certain non-keyframe.
      q.concat(x * rand(50))            # Repeat the frame n times and concatenate with q.
    end

    video = AviGlitch.open q                # New AviGlitch instance using the frames.
  end
end
