def avi_cosa(paths)
  Glitch::Avi::glitch_many('cosa', paths, true) do |video|
    video.glitch do |data|
      data.gsub(/[abcde]/, 'f')
    end
  end
end
