def avi_zero(paths)
  Glitch::Avi::glitch_many('zero', paths) do |video|
    video.glitch(:keyframe) do |data|
      data.gsub(/\d/, '0')
    end
  end
end
