def avi_mosh(paths)
  Glitch::Avi::glitch_many('mosh', paths) do |video|
    video.glitch :keyframe do |f|     # To touch key frames, pass the symbol :keyframe.
      nil                         # Returning nil in glitch method's yield block
    end                           # offers to remove the frame.
  end
end