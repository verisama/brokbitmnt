def avi_saturate(paths)
  Glitch::Avi::transcode_many('saturate', paths) do |video, path|
    video.transcode(path, %w(-vf eq=saturation=3))    
  end
end