module Glitch
  module Avi
    INPUT_DIR  = File.join(SAMPLES_DIR,  'avi')
    OUTPUT_DIR = File.join(GLITCHES_DIR, 'avi')

    CONVERT_CMD = "ffmpeg -i %{input} -vcodec libxvid -qscale 1 -g 100 -me_method epzs -bf 0 -mbd 0 -aspect %{aspect} -acodec copy %{output}"

    INPUT_PATHS = Dir[File.join(INPUT_DIR, '*.avi')]

    class << self
      def glitch_many(name, paths, reencode=false, &block)
        saved_paths = []

        process_many(paths) do |path, index|
          path = convert(path)
          video = AviGlitch.open(path)
          yield video
          saved_paths << save_video(video, name, path, index)
          
          reencode(saved_paths.last) if reencode
        end
        
        return saved_paths
      end
      
      def convert(path)
        ext = File.extname(path)
        if ext.downcase != '.avi'
          avi_name = "#{File.basename(path, ext)}.avi"
          avi_path = File.join(INPUT_DIR, avi_name)
          FFMPEG::Movie.new(path).transcode(avi_path)
          path = avi_path
        end
        path
      end

      def reencode(path)
        movie = FFMPEG::Movie.new(path)
        tmp = path.gsub('.avi', '.mov') 
        movie = movie.transcode(tmp) { |progress| puts progress }
        movie.transcode(path) { |progress| puts progress }
        File.delete(tmp)
      end

      def transcode_many(name, paths, reencode=false, &block)
        process_many(paths) do |path, index|
          video = FFMPEG::Movie.new(path)
          out = out_path(video, name, path, index)
          yield video, out
          reencode(out_path) if reencode
        end
      end
      
      def process_many(paths, &block)
        paths.each_with_index do |path, index|
          puts "Processing #{index + 1} out of #{paths.size}"
          yield path, index
        end
      end

      def out_path(video, name, path, index)
        timestamp = Time.now.strftime('%y%m%d%H%M%S')
        extname   = File.extname(path)
        filename  = [name, '_', index, '_', timestamp, extname].join
        out_path  = File.join(OUTPUT_DIR, filename)
        
        return out_path
      end

      def save_edited_video(video, name, path, index)
        out = out_path(video, name, path, index)
        video.transcode(out)
        return out
      end

      def save_video(video, name, path, index)
        out = out_path(video, name, path, index)
        video.output(out)
        return out
      end
    end
  end
end