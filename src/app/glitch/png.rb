module Glitch
  module Png
    INPUT_DIR  = File.join(SAMPLES_DIR,  'png')
    OUTPUT_DIR = File.join(GLITCHES_DIR, 'png')
    
    FILTERS = { none: 0, sub: 1, up: 2, avg: 3, paeth: 4 }
    
    INPUT_PATHS = Dir[File.join(INPUT_DIR, '*.png')]
    
    class << self
      def glitch_one(name, path, &block)
        process_one(path) do |path|
          image = ChunkyPNG::Image.from_file(path)
          yield image
          return save_image(image, name, path)
        end
      end

      def glitch_many(name, paths, &block)
        saved_paths = []

        process_many(paths) do |path, index|
          image = ChunkyPNG::Image.from_file(path)
          yield image
          saved_paths << save_image(image, name, path, index)
        end

        return saved_paths
      end
      
      def glitch_one_alt(name, path, &block)
        process_one(path) do |path|
          image = PNGlitch.open(path)
          yield image
          return save_image(image, name, path)
        end
      end

      def glitch_many_alt(name, paths, &block)
        saved_paths = []
        
        process_many(paths) do |path, index|
          image = PNGlitch.open(path)
          yield image
          saved_paths << save_image(image, name, path, index)
        end

        return saved_paths
      end


      def process_one(path, &block)
        puts "Processing #{path}"
        yield path
      end

      def process_many(paths, &block)
        paths.each_with_index do |path, index|
          puts "Processing #{index + 1} out of #{paths.size}"
          yield path, index
        end
      end


      def save_image(image, name, path, index)
        timestamp = Time.now.strftime('%y%m%d%H%M%S')
        extname   = File.extname(path)
        filename  = [name, '_', index, '_', timestamp, extname].join
        out_path  = File.join(OUTPUT_DIR, filename)
        
        image.save(out_path)

        return out_path
      end
    end
  end
end