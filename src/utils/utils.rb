require 'digest'

def timestamp(end_expression = 'S', start_expression = 'y')
  format_string = %w(%y %d %m %H %M %S)
  start_idx = format_string.index("%#{end_expression}")
  end_idx   = format_string.index("%#{start_expression}")
  
  format_string = format_string[start_idx..end_idx]
  
  Time.now.strftime(format_string.join)
end

def hex(str, length = nil)
  digest = Digest::SHA256.hexdigest(str)
  length.nil? ? digest : digest[0..(length - 1)]
end

def pvec(x, y = nil)
  if y.nil?
    a = x.to_ary
    x = a[0]
    y = a[1]
  elsif y.is_a? Vector2
    v = y
    x = v.x
    y = v.y
  end
  puts "(#{x}, #{y})"
end
