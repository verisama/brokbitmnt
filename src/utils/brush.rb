require 'matrix'

class Brush
  attr_accessor :size, :string, :matrix
  def initialize(*size, string)
    @size   = size
    @string = string
    
    string_parts = string.split(/[\s\n\r]/)
    
    @matrix = Matrix.build(size[0], size[1]) do |x, y|
      index = x + y * size[0]
      string_parts[index].to_f 
    end
  end
end
