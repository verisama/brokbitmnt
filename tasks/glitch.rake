%w(png avi).each do |type|
  namespace :"#{type}" do
    Dir[File.join(ROOT_PATH, 'src', 'app', 'glitch', type, '*.rb')].each do |file|
      script = File.basename(file, '.rb')
      desc "Run glitch #{type} script #{script}"
      task script do |t, args|
        puts "Running #{file}"
        require_relative file
      end
    end
  end
end
