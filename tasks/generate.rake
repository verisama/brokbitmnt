task gen: ['gen:last']

namespace :gen do
  
  Dir[File.join(ROOT_PATH, 'scripts', 'generate', '*')].sort.reverse.each_with_index do |file, index|
    if index.zero?
      desc 'Run last experiment'
      task :last do |t, args|
        puts "Running #{file}"
        require_relative file
      end
    end
    
    n = file.scan(/\d+/).first
    desc "Run 'generate' experiment #{n}"
    task n do |t, args|
      puts "Running #{file}"
      require_relative file
    end
  end
end
