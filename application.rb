require 'mini_magick'
require 'chunky_png'
require 'pnglitch'
require 'aviglitch'
require 'matrix'
require 'moshy'
require 'streamio-ffmpeg'

ROOT_PATH = File.expand_path(__dir__)

GLITCHES_DIR = File.expand_path(File.join(ROOT_PATH, 'data/glitches'))
SAMPLES_DIR  = File.expand_path(File.join(ROOT_PATH, 'data/samples'))

Dir[File.join(ROOT_PATH, 'src', 'extensions', '**', '*.rb')].each do |file|
  require file
end

Dir[File.join(ROOT_PATH, 'src', 'utils', '**', '*.rb')].each do |file|
  require file
end

Dir[File.join(ROOT_PATH, 'src', 'app', 'glitch', '*.rb')].each do |file|
  require file
end
